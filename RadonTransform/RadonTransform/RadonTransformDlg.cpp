﻿
// RadonTransformDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "RadonTransform.h"
#include "RadonTransformDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CRadonTransformDlg



CRadonTransformDlg::CRadonTransformDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_RADONTRANSFORM_DIALOG, pParent)
	, s_dots(16)
	, tetta_dots(16)
	, iter_value(100000)
	, alfa(0.15)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CRadonTransformDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, s_dots);
	DDX_Text(pDX, IDC_EDIT2, tetta_dots);
	DDX_Text(pDX, IDC_EDIT3, iter_value);
	DDX_Text(pDX, IDC_EDIT4, alfa);
}

BEGIN_MESSAGE_MAP(CRadonTransformDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CRadonTransformDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDCANCEL, &CRadonTransformDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON3, &CRadonTransformDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CRadonTransformDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON2, &CRadonTransformDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// Обработчики сообщений CRadonTransformDlg

BOOL CRadonTransformDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	hp.Init("white_round.jpg");

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CRadonTransformDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CRadonTransformDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CRadonTransformDlg::OnBnClickedButton1()
{	
	UpdateData(1);
	//hp.Init("white_round.jpg");
	//hp.Init("white_square.jpg");
	hp.Init("pic.jpg");
	hp.show_image();
	hp.RadonTransform1(s_dots, tetta_dots);
	hp.show_s_tetta();
	UpdateData(0);
}

void CRadonTransformDlg::OnBnClickedButton3()
{
	hp.Back_RadonTransform1();
	hp.show_test_normal();
}

void CRadonTransformDlg::OnBnClickedButton2()
{
	UpdateData(1);
	//hp.Init("white_round.jpg");
	//hp.Init("white_square.jpg");
	hp.Init("pic32.jpg");
	hp.show_image();
	hp.RadonTransform2(s_dots, tetta_dots);
	hp.show_s_tetta();
	UpdateData(0);
}

void CRadonTransformDlg::OnBnClickedButton4()
{
	UpdateData(1);
	hp.iterations_value = iter_value;
	hp.alfa = alfa;
	hp.Back_RadonTransform2();
	hp.show_test_normal();
	UpdateData(0);
}

void CRadonTransformDlg::OnBnClickedCancel()
{
	// TODO: добавьте свой код обработчика уведомлений
	CDialogEx::OnCancel();
}

