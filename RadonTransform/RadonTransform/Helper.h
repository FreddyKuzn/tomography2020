#pragma once
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp> 
#include <opencv2/features2d/features2d.hpp>
#include <memory>
#include <cstdint>
#include <vector>
#include <string>
#define _USE_MATH_DEFINES
#include <math.h>
using namespace cv;
using namespace std;

class Helper
{
private:
	string path; //���� �� �����������
	Mat imag, white_line, cross_imag;
	Point2f img_c;//���������� ������ �����������
	vector<Point> gran_dots_v;//������ ����� ����������� (0;0) - ����� �����������
	double er = 0.001;
	double s_min = -1, s_max = 1, tetta_min = 0, tetta_max = 1;
	double tetta_step, s_step;
	int s_dots;
	int tetta_dots;
	Mat S_Tetta; Mat test_pic;
	/*TASK II*/
	Mat Y;
	Mat A;
public:
	int iterations_value = 100;
	float alfa = 1.;
	float first, second;
	

protected:
	inline bool are_crossing(double k0, double b0, double k, double b, Point& p);
	inline bool are_crossing(double k0, double b0, double z, Point& p);
	inline int step2(int sizein);//���������� ��������� �������� pow(2,n)
public:

	Helper(string path) : path(path)
	{
		Init(path);
	};
	Helper() {};
	virtual ~Helper() {};

	void Init(string path)
	{
		this->path = path;
		imag = imread(path, IMREAD_COLOR);
		cvtColor(imag, imag, COLOR_BGR2GRAY);
		img_c.x = imag.rows / 2;
		img_c.y = imag.cols / 2;
		white_line = imag * 0;		
	}
	void show_image()
	{
		if (!imag.empty())
		{
			namedWindow("imag", WINDOW_NORMAL);
			imshow("imag", imag);
		}
	}
	void show_s_tetta()
	{

		if (!S_Tetta.empty())
		{
			namedWindow("S_Tetta", WINDOW_NORMAL);
			imshow("S_Tetta", S_Tetta);
		}

	}
	void show_test_normal()
	{
		if (!test_pic.empty())
		{
			namedWindow("test_pic", WINDOW_NORMAL);
			imshow("test_pic", test_pic);
		}
	}
	void show_test_auto()
	{
		if (!test_pic.empty())
		{
			namedWindow("test_pic", WINDOW_AUTOSIZE);
			imshow("test_pic", test_pic);
		}
	}
	cv::Mat line(double s, double tetta, cv::Scalar &sc);
	void RadonTransform1(int s_dots, int tetta_dots);
	void Back_RadonTransform1();
	void RadonTransform2(int s_dots, int tetta_dots);
	void Back_RadonTransform2();
	void MONTE_CARLO(Mat& A, Mat& X, Mat& Y, int N, int iterations_value, float alfa);
	void NEW_MONTE_CARLO(Mat &A, Mat& X, Mat &Y, int N, int iterations_value, float alfa);
};


