﻿
// RadonTransform.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CRadonTransformApp:
// Сведения о реализации этого класса: RadonTransform.cpp
//

class CRadonTransformApp : public CWinApp
{
public:
	CRadonTransformApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CRadonTransformApp theApp;
