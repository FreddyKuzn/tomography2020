#include "pch.h"
#include "Helper.h"

void Helper::MONTE_CARLO(Mat& A, Mat& X, Mat& Y, int N, int iterations_value, float alfa)
{
	X.release();
	X = Mat::zeros(N, 1, CV_32F);
	for (int i = 0; i < N; i++)
		X.at<float>(i, 0) = (float)((float)rand() / RAND_MAX);
	//������ "������"
	Mat Q;
	Mat Axy = A * X - Y;
	Mat AxyT;
	cv::transpose(Axy, AxyT);
	Q = AxyT * Axy;

	//���������� ����
	for (int i = 0; i < iterations_value; i+=4)
	{
		//��������� �������
		Mat X_add = Mat::zeros(N, 1, CV_32F);
		for (int j = 0; j < N; j++)
		{
			float add = (-1. + ((float)rand() / RAND_MAX) * 2.);
			X_add.at<float>(j, 0) = add;
		}
		X_add *= alfa;
		Mat X_buffer = X + X_add;
		//������ "������"
		Axy = A * X_buffer - Y;
		cv::transpose(Axy, AxyT);
		Mat Q_buffer = AxyT * Axy;

		//this->first = Q_buffer.at<float>(0, 0);
		//this->second = Q.at<float>(0, 0);
		if (Q_buffer.at<float>(0, 0) < Q.at<float>(0, 0))
		{
			X = X_buffer(Rect(0, 0, X_buffer.cols, X_buffer.rows));
			Q = Q_buffer;
		}
		X_buffer.release();
		X_add.release();
		Q_buffer.release();
	}
}
void Helper::NEW_MONTE_CARLO(Mat &A, Mat& X, Mat &Y, int N, int iterations_value, float alfa)
{
	X.release();
	X = cv::Mat::zeros(N, 1, CV_32F);
	for (int i = 0; i < N; i++)
		X.at<float>(i, 0) = (float)((float)rand() / RAND_MAX) * 10.;
	//������ "������"
	Mat Q;
	Mat Axy = A * X - Y;
	Mat AxyT;
	cv::transpose(Axy, AxyT);
	Q = AxyT * Axy;
	//���������� ����
	vector<Mat> X_add_vec; X_add_vec.resize(N);
	for (auto i : X_add_vec) i = Mat::zeros(N, 1, CV_32F);
	for (int i = 0; i < iterations_value; i++)
	{		
#pragma omp parallel shared(alfa,A,Y,X_add_vec,X,Q) private (Axy,AxyT)
		{
#pragma omp for 
			for (int j = 0; j < N; j++)
			{
				X_add_vec[j] = Mat::zeros(N, 1, CV_32F);
				//��������� �������
				Mat X_add = Mat::zeros(N, 1, CV_32F);
				float add = (-1. + ((float)rand() / RAND_MAX) * 2.);
				X_add.at<float>(j, 0) = add * alfa;
				Mat X_buffer = X + X_add;

				//������ "������"
				Axy = A * X_buffer - Y;
				cv::transpose(Axy, AxyT);
				Mat Q_buffer = AxyT * Axy;
				if (Q_buffer.at<float>(0, 0) < Q.at<float>(0, 0))
				{
					X_add_vec[j] = X_add;
				}
			}
		}
		Mat X_add = Mat::zeros(N, 1, CV_32F);
		for (auto a : X_add_vec)
			if (!a.empty())X_add += a;
		Mat X_buffer = X + X_add;
		//������ "������"
		Axy = A * X_buffer - Y;
		cv::transpose(Axy, AxyT);
		Mat Q_buffer = AxyT * Axy;
		X = X_buffer(Rect(0, 0, X_buffer.cols, X_buffer.rows));
		Q = Q_buffer;
	}
}
inline bool Helper::are_crossing(double k0, double b0, double k, double b, Point& p)
{
	if (abs(k - k0) < er) return false;

	p.x = (b - b0) / (k0 - k);
	p.y = k0 * p.x + b0;
	return true;
}
inline bool Helper::are_crossing(double k0, double b0, double z, Point& p)
{
	if (abs(k0) > (1. / er))
	{
		p.x = z;
		p.y = b0;
		return false;
	}
	else
	{
		p.x = z;
		p.y = k0 * p.x + b0;
		return true;
	}
}
cv::Mat Helper::line(double s, double tetta, cv::Scalar& sc)
{
	white_line = imag * 0;
	gran_dots_v.clear();

	double k, b;
	tetta *= -M_PI;
	s *= sqrt(pow(img_c.x, 2) + pow(img_c.y, 2));
	double alfa = (M_PI / 2) - tetta;
	Point gran;
	k = -tan(alfa);
	b = s * sin(tetta) - k * s * cos(tetta);

	if (are_crossing(k, b, img_c.x, gran)) gran_dots_v.push_back(gran);
	if (are_crossing(k, b, -img_c.x, gran)) gran_dots_v.push_back(gran);
	if (are_crossing(k, b, 0., img_c.y, gran))gran_dots_v.push_back(gran);
	if (are_crossing(k, b, 0., -img_c.y, gran))gran_dots_v.push_back(gran);
	if (abs(k) > (1. / er))
	{
		gran.x = s; gran.y = img_c.y;
		gran_dots_v.push_back(gran);
		gran.x = s; gran.y = -img_c.y;
		gran_dots_v.push_back(gran);
	}
	for (int i = 0; i < gran_dots_v.size(); i++)
	{
		gran_dots_v[i].x += img_c.x;
		gran_dots_v[i].y += img_c.y;
	}
	if (gran_dots_v.size() >= 2)
	{
		if (abs(tetta) < er) cv::line(white_line, gran_dots_v[2], gran_dots_v[3], Scalar(1, 1, 1), 1);
		else
			cv::line(white_line, gran_dots_v[1], gran_dots_v[0], Scalar(1, 1, 1), 1);
	}
	cross_imag = imag.mul(white_line);
	sc = cv::sum(cross_imag);
	return white_line;
}
void Helper::RadonTransform1(int s_dots_, int tetta_dots_)
{
	S_Tetta.release();
	this->s_dots = s_dots_;
	this->tetta_dots = tetta_dots_;
	s_step = (s_max - s_min) / this->s_dots;
	tetta_step = (tetta_max - tetta_min) / this->tetta_dots;
	S_Tetta = Mat::zeros(s_dots, tetta_dots, CV_32FC3);
	for (int i = 0; i < tetta_dots; i++)
	{
		for (int j = 0; j < s_dots; j++)
		{
			double tetta_now = tetta_min + i * tetta_step;
			double s_now = s_min + j * s_step;
			Scalar buf_pixel;
			line(s_now, tetta_now, buf_pixel);
			S_Tetta.at<Vec3f>(i, j)[0] = (double)(buf_pixel[0]);
			S_Tetta.at<Vec3f>(i, j)[1] = (double)(buf_pixel[0]);
			S_Tetta.at<Vec3f>(i, j)[2] = (double)(buf_pixel[0]);
		}
	}
	normalize(S_Tetta, S_Tetta, 1.0, 0.0, NORM_MINMAX);
}
void Helper::Back_RadonTransform1()
{
	test_pic.release();
	if (S_Tetta.empty() || \
		S_Tetta.rows != step2(S_Tetta.rows) || \
		S_Tetta.cols != step2(S_Tetta.cols))
		return;
	//////////////////////////////////////////////////
	//////////////////////////////////////////////////
	Mat buffer;
	buffer = Mat::zeros(S_Tetta.rows, S_Tetta.cols, CV_32FC2);
	for (int i = 0; i < S_Tetta.rows; i++)
	{
		for (int j = 0; j < S_Tetta.cols; j++)
		{
			buffer.at<Vec2f>(i, j)[0] = S_Tetta.at<cv::Vec3f>(i, (j + S_Tetta.cols / 2) % S_Tetta.cols)[0];
		}
	}
	//////////////////////////////////////////////////
	//////////////////////////////////////////////////
	cv::dft(buffer, buffer, cv::DFT_ROWS | cv::DFT_SCALE | cv::DFT_COMPLEX_OUTPUT);
	//////////////////////////////////////////////////
	//////////////////////////////////////////////////
	Mat left = buffer(Rect(0, 0, buffer.cols / 2, buffer.rows));
	Mat right = buffer(Rect(buffer.cols / 2, 0, buffer.cols / 2, buffer.rows));
	Mat help_mat = left.clone();
	right.copyTo(left);
	help_mat.copyTo(right);
	cv::hconcat(left, right, buffer);
	left.release(); right.release(); help_mat.release();
	//////////////////////////////////////////////////
	//////////////////////////////////////////////////
	left = buffer(Rect(0, 0, (buffer.cols / 2), buffer.rows));
	right = buffer(Rect(buffer.cols / 2, 0, buffer.cols / 2, buffer.rows));
	cv::flip(left, left, 1);
	left = left(Rect(0, 0, left.cols - 1, left.rows));
	Mat dop_mat = right(Rect(0, 0, 1, right.rows));
	cv::hconcat(dop_mat, left, left);
	cv::vconcat(left, right, buffer);
	//////////////////////////////////////////////////
	//////////////////////////////////////////////////
	Point2f center((float)buffer.cols / 2, (float)buffer.rows / 2);
	double maxRadius = 0.7 * min(buffer.rows, buffer.cols);
	int flags = INTER_LINEAR + WARP_INVERSE_MAP + WARP_FILL_OUTLIERS;
	cv::linearPolar(buffer, buffer, center, maxRadius, flags);
	////////////////////////////////////////////////
	////////////////////////////////////////////////
	cv::dft(buffer, buffer, cv::DFT_INVERSE | cv::DFT_COMPLEX_OUTPUT);
	for (int i = 0; i < buffer.rows / 2; i++)
	{
		for (int j = 0; j < buffer.cols / 2; j++)
		{
			//������ ii � iv
			swap(buffer.at<Vec2f>(i, j), buffer.at<Vec2f>(i + buffer.rows / 2, j + buffer.cols / 2));
			//������ i � iii
			swap(buffer.at<Vec2f>(i + buffer.rows / 2, j), buffer.at<Vec2f>(i, j + buffer.cols / 2));
		}
	}
	cv::flip(buffer, buffer, 1);
	////////////////////////////////////////////////
	////////////////////////////////////////////////
	test_pic = Mat::zeros(buffer.rows, buffer.cols, CV_32FC3);
	for (int i = 0; i < test_pic.rows; i++)
	{
		for (int j = 0; j < test_pic.cols; j++)
		{
			double real = buffer.at<cv::Vec2f>(i, j)[0];
			double image = buffer.at<cv::Vec2f>(i, j)[1];

			double data = sqrt(pow(real, 2) + pow(image, 2));
			test_pic.at<Vec3f>(i, j)[0] = sqrt(pow(real, 2) + pow(image, 2));
			test_pic.at<Vec3f>(i, j)[1] = test_pic.at<Vec3f>(i, j)[0];
			test_pic.at<Vec3f>(i, j)[2] = test_pic.at<Vec3f>(i, j)[0];
		}
	}
	buffer.release();
}
void Helper::RadonTransform2(int s_dots_, int tetta_dots_)
{
	S_Tetta.release();
	this->s_dots = s_dots_;
	this->tetta_dots = tetta_dots_;
	s_step = (s_max - s_min) / this->s_dots;
	tetta_step = (tetta_max - tetta_min) / this->tetta_dots;
	S_Tetta = Mat::zeros(s_dots, tetta_dots, CV_32FC3);
	//A.resize(s_dots_ * tetta_dots_);
	A = Mat::zeros(s_dots * tetta_dots, this->imag.rows * this->imag.cols, CV_32F);
	Y.release();
	Y = Mat::zeros(s_dots * tetta_dots, 1, CV_32F);

	for (int i = 0; i < tetta_dots; i++)
	{
		for (int j = 0; j < s_dots; j++)
		{
			double tetta_now = tetta_min + i * tetta_step;
			double s_now = s_min + j * s_step;
			Scalar buf_pixel;
			Mat line_now;
			line_now = line(s_now, tetta_now, buf_pixel);
			S_Tetta.at<Vec3f>(i, j)[0] = (float)(buf_pixel[0]);
			S_Tetta.at<Vec3f>(i, j)[1] = (float)(buf_pixel[0]);
			S_Tetta.at<Vec3f>(i, j)[2] = (float)(buf_pixel[0]);
			for (int m = 0; m < imag.rows; m++)
				for (int n = 0; n < imag.cols; n++)
				{
					A.at<float>(i * s_dots + j, m * imag.cols + n) = line_now.at<uchar>(m, n);
				}
		}
	}
	//normalize(S_Tetta, S_Tetta, 0.0, 1.0, NORM_MINMAX);
	for (int i = 0; i < tetta_dots; i++)
		for (int j = 0; j < s_dots; j++)
			Y.at<float>(i * s_dots + j, 0) = S_Tetta.at<Vec3f>(i, j)[0];
}

void Helper::Back_RadonTransform2()
{
	int m_m = s_dots * tetta_dots;
	int n_n = this->imag.rows * this->imag.cols;
	Mat X;
	NEW_MONTE_CARLO(A, X, Y, n_n, iterations_value, alfa);
	normalize(X, X, 0.0, 1.0, NORM_MINMAX);
	//����� �� �����
	test_pic.release();
	/*test_pic = Y;*/
	test_pic = Mat::zeros(S_Tetta.rows, S_Tetta.cols, CV_32F);
	for (int i = 0; i < test_pic.rows; i++)
		for (int j = 0; j < test_pic.cols; j++) test_pic.at<float>(i, j) = X.at<float>(i * test_pic.cols + j, 0);
}
inline int Helper::step2(int sizein)
{
	int i = 0;
	double S = sizein;
	for (;;)
	{
		if (S > 1)
		{
			i++;
			S /= 2;
		}
		else break;
	}
	return pow(2, i);
}
