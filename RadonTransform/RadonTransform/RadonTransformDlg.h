﻿
// RadonTransformDlg.h: файл заголовка
//

#pragma once

#include "Helper.h"


using namespace std;

// Диалоговое окно CRadonTransformDlg
class CRadonTransformDlg : public CDialogEx
{
// Создание
public:
	CRadonTransformDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_RADONTRANSFORM_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedCancel();
	Helper hp;
	int s_dots;
	int tetta_dots;
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton2();
	int iter_value;
	float alfa;
};
