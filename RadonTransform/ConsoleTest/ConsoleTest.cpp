﻿// ConsoleTest.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
#pragma once
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp> 
#include <opencv2/features2d/features2d.hpp>
#include <memory>
#include <cstdint>
#include <vector>
#include <string>
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

using namespace cv;
using namespace std;

void MONTE_CARLO(Mat& A, Mat& X, Mat& Y, int N, int iterations_value, float alfa)
{
	X.release();
	X = cv::Mat::zeros(N, 1, CV_32F);
	for (int i = 0; i < N; i++)
		X.at<float>(i, 0) = (float)((float)rand() / RAND_MAX) * 10.;

	//Расчёт "ошибки"
	Mat Q;
	Mat Axy = A * X - Y;
	Mat AxyT;
	cv::transpose(Axy, AxyT);
	Q = AxyT * Axy;

	//Марковская цепь
	for (int i = 0; i < iterations_value; i++)
	{
		//Случайная добавка
		Mat X_add = Mat::zeros(N, 1, CV_32F);
		for (int i = 0; i < N; i++)
		{
			float add = (-1. + ((float)rand() / RAND_MAX) * 2.);
			X_add.at<float>(i, 0) = add;
		}
		X_add *= alfa;
		Mat X_buffer = X + X_add;
		//Расчёт "ошибки"
		Axy = A * X_buffer - Y;
		cv::transpose(Axy, AxyT);
		Mat Q_buffer = AxyT * Axy;

		//this->first = Q_buffer.at<float>(0, 0);
		//this->second = Q.at<float>(0, 0);
		if (Q_buffer.at<float>(0, 0) < Q.at<float>(0, 0))
		{
			X = X_buffer(Rect(0, 0, X_buffer.cols, X_buffer.rows));
			Q = Q_buffer;
		}
		X_buffer.release();
		X_add.release();
		Q_buffer.release();
	}
}

int main()
{
	int N = 4;
	Mat A; A = cv::Mat::zeros(N, N, CV_32F);
	A.at<float>(0, 0) = 1.;
	A.at<float>(0, 1) = -1.;
	A.at<float>(0, 2) = 3.;
	A.at<float>(0, 3) = 1.;

	A.at<float>(1, 0) = 4.;
	A.at<float>(1, 1) = -1.;
	A.at<float>(1, 2) = 5.;
	A.at<float>(1, 3) = 4.;

	A.at<float>(2, 0) = 2.;
	A.at<float>(2, 1) = -2.;
	A.at<float>(2, 2) = 4.;
	A.at<float>(2, 3) = 1.;

	A.at<float>(3, 0) = 1.;
	A.at<float>(3, 1) = -4.;
	A.at<float>(3, 2) = 5.;
	A.at<float>(3, 3) = -1.;
	cout << "Matr A:" << endl;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			cout << A.at<float>(i, j) << "  ";
		}
		cout << endl;
	}
	Mat Y; Y = cv::Mat::zeros(N, 1, CV_32F);
	Y.at<float>(0, 0) = 5.;
	Y.at<float>(0, 1) = 4.;
	Y.at<float>(0, 2) = 6.;
	Y.at<float>(0, 3) = 3.;
	cout << "Vector Y:" << endl;
	for (int i = 0; i < N; i++)
		cout << Y.at<float>(0, i) << "  ";
	cout << endl;
	Mat X;
	///////МОНТЕ-КАРЛО///////////
	MONTE_CARLO(A, X, Y, N, 100000, 0.15);
	cout << "Vector X:" << endl;
	for (int i = 0; i < N; i++)
		cout << X.at<float>(0, i) << "  ";
	cout << endl;

	cout << "Real vector X:" << endl;
	cout << 9 << "  " << 18 << "  " << 10 << "  " << -16 << endl;

}